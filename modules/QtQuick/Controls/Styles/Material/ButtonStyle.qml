/*
 * QML Material - An application framework implementing Material Design.
 * Copyright (C) 2015 Michael Spencer <sonrisesoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Controls.Styles 1.3
import Material 0.2

ButtonStyle {
    id: style

    padding {
        left: 0
        right: 0
        top: 0
        bottom: 0
    }

    readonly property int controlElevation: 1
    readonly property bool __hasBackgroundColor: control.hasOwnProperty("backgroundColor")

    property color controlBackground: __hasBackgroundColor ? control.backgroundColor : "white"

    readonly property string context: control.hasOwnProperty("context") ? control.context : "default"

    background: View {
        id: background

        implicitHeight: Units.dp(36)

        radius: Units.dp(2)

        backgroundColor: control.enabled || controlElevation === 0
                ? controlBackground
                : Qt.rgba(0, 0, 0, 0.12)

        elevation: {
            var elevation = controlElevation

            if (elevation > 0 && control.focus)
                elevation++;

            if(!control.enabled)
                elevation = 0;

            return elevation;
        }

        Ink {
            id: mouseArea

            anchors.fill: parent

            Connections {
                target: control.__behavior
                onPressed: mouseArea.onPressed(mouse)
                onCanceled: mouseArea.onCanceled()
                onReleased: mouseArea.onReleased(mouse)
            }
        }
    }
    label: Item {
        implicitHeight: Math.max(Units.dp(36), label.height + Units.dp(16))
        implicitWidth: context == "dialog"
                ? Math.max(Units.dp(64), label.width + Units.dp(16))
                : context == "snackbar" ? label.width + Units.dp(16)
                                        : Math.max(Units.dp(88), label.width + Units.dp(32))

        LabelButton {
            id: label
            anchors.centerIn: parent
            text: control.text
            color: control.enabled ? control.hasOwnProperty("textColor")
                                     ? control.textColor : Theme.light.textColor
                    : Qt.rgba(0, 0, 0, 0.26)
        }
    }
}
