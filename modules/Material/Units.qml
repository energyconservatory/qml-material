/*
 * QML Material - An application framework implementing Material Design.
 * Copyright (C) 2014-2015 Michael Spencer <sonrisesoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

pragma Singleton

/*!
   \qmltype Units
   \inqmlmodule Material

   \brief Provides access to screen-independent Units known as DPs (device-independent pixels).

   This singleton provides methods for building a user interface that automatically scales based on
   the screen density. Use the \l Units::dp function wherever you need to specify a screen size,
   and your app will automatically scale to any screen density.

   Here is a short example:

   \qml
   import QtQuick 2.4
   import Material 0.2

   Rectangle {
       width: Units.dp(100)
       height: Units.dp(80)

       Label {
           text:"A"
           font.pixelSize: Units.dp(50)
       }
   }
   \endqml
*/
QtObject {
    id: units

    /*!
       This is the standard function to use for accessing device-independent pixels. You should use
       this anywhere you need to refer to distances on the screen.
       Use cheap(er) rounding.
     */
    function dp(number) {
        return (number * 0.7936 + 0.5) | 0;
    }

    function gu(number) {
        return number * gridUnit
    }

    property int gridUnit: dp(64)
}
