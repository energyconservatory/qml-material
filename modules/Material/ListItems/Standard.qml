/*
 * QML Material - An application framework implementing Material Design.
 * Copyright (C) 2014-2015 Michael Spencer <sonrisesoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.1
import Material 0.2

/*!
   \qmltype Standard
   \inqmlmodule Material.ListItems

   \brief A simple list item with a single line of text and optional primary and secondary actions.
 */
BaseListItem {
    id: listItem

    dividerInset: 0
    implicitHeight: Units.dp(45)
    height: Units.dp(45)

    property alias text: label.text
    property alias textColor: label.color

    LabelSubheading {
        id: label
        anchors {
            fill: parent
            leftMargin: Units.dp(16)
            rightMargin: Units.dp(16)
        }
        elide: Text.ElideRight
        color: listItem.selected ? Theme.primaryColor : Theme.light.textColor
        verticalAlignment: Text.AlignVCenter
    }
}
