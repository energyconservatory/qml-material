/*
 * QML Material - An application framework implementing Material Design.
 * Copyright (C) 2014-2015 Michael Spencer <sonrisesoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import QtQuick.Controls 1.3 as Controls
import QtQuick.Window 2.2
import Material 0.2
import Material.Extras 0.1

/*!
   \qmltype ApplicationWindow
   \inqmlmodule Material

   \brief A window that provides features commonly used for Material Design apps.

   This is normally what you should use as your root component. It provides a \l Toolbar and
   \l PageStack to provide access to standard features used by Material Design applications.

   Here is a short working example of an application:

   \qml
   import QtQuick 2.4
   import Material 0.2

   ApplicationWindow {
       title: "Application Name"

       initialPage: page

       Page {
           id: page
           title: "Page Title"

           Label {
               anchors.centerIn: parent
               text: "Hello World!"
           }
       }
   }
   \endqml
*/
Controls.ApplicationWindow {
    id: app

    /*!
       \qmlproperty Page initialPage

       The initial page shown when the application starts.
     */
    property alias initialPage: __pageStack.initialItem

    /*!
       \qmlproperty PageStack pageStack

       The \l PageStack used for controlling pages and transitions between pages.
     */
    property alias pageStack: __pageStack

    /*!
       \qmlproperty AppTheme theme

       A grouped property that allows the application to customize the the primary color, the
       primary dark color, and the accent color. See \l Theme for more details.
     */
    property alias theme: __theme

	property int leftFrameSize: 0
	property int rightFrameSize: 0
	property int topFrameSize: 0
	property int bottomFrameSize: 0

    property int toolbarTopMargin: 0

    property Component statusCenterComponent
    property alias statusCenterLoader: __statusCenterLoader

    AppTheme {
        id: __theme
    }

    PlatformExtensions {
        id: platformExtensions
        decorationColor: __toolbar.decorationColor
        window: app
    }

    PageStack {
        id: __pageStack
        anchors {
            left: parent.left
			leftMargin: leftFrameSize
            right: parent.right
			rightMargin: rightFrameSize
            top: __toolbar.bottom
            bottom: parent.bottom
			bottomMargin: bottomFrameSize
        }
        onPushed: __toolbar.push(page)
        onPopped: __toolbar.pop(page)
        onReplaced: __toolbar.replace(page)
    }

    Toolbar {
        id: __toolbar
		anchors {
            left: parent.left
			leftMargin: leftFrameSize
            right: parent.right
			rightMargin: rightFrameSize
			top: parent.top
            topMargin: topFrameSize + toolbarTopMargin
        }
    }

    Loader {
        id: __statusCenterLoader
        active: false
        anchors {
            left: parent.left
            leftMargin: leftFrameSize
            right: parent.right
            rightMargin: rightFrameSize
            top: parent.top
            topMargin: topFrameSize
        }
        sourceComponent: statusCenterComponent
    }

    OverlayLayer {
        id: dialogOverlayLayer
        objectName: "dialogOverlayLayer"
		anchors {
            left: parent.left
			leftMargin: leftFrameSize
            right: parent.right
			rightMargin: rightFrameSize
			top: parent.top
			topMargin: topFrameSize
            bottom: parent.bottom
			bottomMargin: bottomFrameSize
        }
    }

    OverlayLayer {
        id: tooltipOverlayLayer
        objectName: "tooltipOverlayLayer"
		anchors {
            left: parent.left
			leftMargin: leftFrameSize
            right: parent.right
			rightMargin: rightFrameSize
			top: parent.top
			topMargin: topFrameSize
            bottom: parent.bottom
			bottomMargin: bottomFrameSize
        }
    }

    OverlayLayer {
        id: overlayLayer
		anchors {
            left: parent.left
			leftMargin: leftFrameSize
            right: parent.right
			rightMargin: rightFrameSize
			top: parent.top
			topMargin: topFrameSize
            bottom: parent.bottom
			bottomMargin: bottomFrameSize
        }
    }

    // Downloaded from https://codeload.github.com/google/material-design-icons/zip/2.2.0
    FontLoader { id: materialFont; source: Qt.resolvedUrl("fonts/material/MaterialIcons-Regular.ttf") }

    // Downloaded from https://materialdesignicons.com - v1.6.50
    // https://codeload.github.com/Templarian/MaterialDesign-Webfont/zip/master
    FontLoader { id: materialFontAll; source: Qt.resolvedUrl("fonts/material/materialdesignicons-webfont.ttf") }

    FontLoader { id: fontAwesome; source: Qt.resolvedUrl("fonts/fontawesome/FontAwesome.otf") }

    width: Units.dp(800)
    height: Units.dp(600)

    Component.onCompleted: {
        function calculateDiagonal() {
            return Math.sqrt(Math.pow((Screen.width/Screen.pixelDensity), 2) +
                    Math.pow((Screen.height/Screen.pixelDensity), 2)) * 0.039370;
        }

        Device.type = Qt.binding(function () {
            var diagonal = calculateDiagonal();

            if (diagonal >= 3.5 && diagonal < 5) { //iPhone 1st generation to phablet
                return Device.phone;
            } else if (diagonal >= 5 && diagonal < 6.5) {
                return Device.phablet;
            } else if (diagonal >= 6.5 && diagonal < 10.1) {
                return Device.tablet;
            } else if (diagonal >= 10.1 && diagonal < 29) {
                return Device.desktop;
            } else if (diagonal >= 29 && diagonal < 92) {
                return Device.tv;
            } else {
                return Device.unknown;
            }
        });

        // Nasty hack because singletons cannot import the module they were declared in, so
        // the grid unit cannot be defined in either Device or Units, because it requires both.
        Units.gridUnit = (function() {
            return Device.type === Device.phone || Device.type === Device.phablet
                    ? Units.dp(48) : Device.type == Device.tablet ? Units.dp(56) : Units.dp(64)
        })()
    }
}
