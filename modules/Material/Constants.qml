/*
 * QML Material - An application framework implementing Material Design.
 * Copyright (C) 2014-2015 Michael Spencer <sonrisesoftware@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import 'awesome.js' as Awesome

pragma Singleton

/*!
   \qmltype Constants
   \inqmlmodule Material

   \brief A singleton that provides constants for internal use.
 */
QtObject {
    readonly property var awesomeIcons: Awesome.map
    readonly property var fontStyles: {
        "title": {
            "size": 20,
            "font": "medium"
        },
        "dialog": {
            "size": 18,
            "font": "regular"
        },
        "subheading": {
            "size": 16,
            "font": "regular"
        },
        "body2": {
            "size": 14,
            "font": "medium"
        },
        "body1": {
            "size": 14,
            "font": "regular"
        },
        "caption": {
            "size": 12,
            "font": "regular"
        }
    }

}
